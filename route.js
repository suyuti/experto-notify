const routes = require('express').Router();

const notify = require('./notify');
routes.use('/notify', notify);

module.exports = routes;
