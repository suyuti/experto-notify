var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('./certs/server.key', 'utf8');
var certificate = fs.readFileSync('./certs/server.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};



const axios = require("axios");
var querystring = require("querystring");
var express = require("express");
require("dotenv").config();
const bodyParser = require("body-parser");
const cors = require("cors");

var app = express();
app.set("port", process.env.PORT || 4100);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "200mb" }));
app.use(cors());
//app.use(require("./route.js"));


app.get('/notify', (req, res) => {
    console.log('here')
    return res.status(200).json({})
})

//app.listen(app.get("port"));
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(8080);
httpsServer.listen(8443);

const tenantId = process.env.TENANT_ID
const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET
const notifyRedirectUri = process.env.NOTIFY_REDIRECT


axios.post(
    `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
    querystring.stringify({
        client_id: clientId,
        scope: "https://graph.microsoft.com/.default",
        client_secret: clientSecret,
        grant_type: "client_credentials",
    }),
    {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
    }
).then(resp => {
    console.log(resp)
    axios.post(
        `https://graph.microsoft.com/v1.0/subscriptions`,
        {
            changeType: "created",
            notificationUrl: notifyRedirectUri,
            resource: "users/1c88de8b-4d0d-4f59-817c-61f2b76438f0/messages",
            expirationDateTime: "2021-12-31T18:23:45.9356913Z",
            clientState: "secretClientValue",
            latestSupportedTlsVersion: "v1_2"
        },
        { headers: { Authorization: "Bearer " + resp.data.access_token } }
    ).then(r => console.log(r))
    .catch(e => console.log(e.response))

})
